<?php 
/**
* 
*/
class Producto
{
	public $nombre;
	public $referencia;
	public $invetario;
	public $precio;
	public $delete_at;

	public static function save($producto){
		$db=Db::getConnect();

		$insert=$db->prepare('INSERT INTO producto VALUES (NULL,:nombre,:referencia,:inventario,:precio,:peso,:categoria,NULL,:deleted_at)');
		$insert->bindValue('nombre',$producto->nombre);
		$insert->bindValue('referencia',$producto->referencia);
		$insert->bindValue('inventario',$producto->inventario);
		$insert->bindValue('precio',$producto->precio);
		$insert->bindValue('peso',$producto->peso);
		$insert->bindValue('categoria',$producto->categoria);
		$insert->bindValue('deleted_at',NULL);
		$insert->execute();
	}

	public static function all(){
		$db=Db::getConnect();
		$listaProducto=[];

		$sql = 'SELECT* FROM producto order by id';
		$resultado = $db->prepare($sql);
		$resultado->execute();
		$listaProductos=$resultado->fetchAll(PDO::FETCH_ASSOC);
		return $listaProductos;
	}

	public static function searchById($id){
		$db=Db::getConnect();
		$Producto=[];

		$sql = "SELECT* FROM producto WHERE id = $id";
		$resultado = $db->prepare($sql);
		$resultado->execute();
		$Producto = $resultado->fetchAll(PDO::FETCH_ASSOC);
		return $Producto;
	}

	public static function update($producto){
		$db=Db::getConnect();
		$id = $producto->id;
		$update=$db->prepare("UPDATE producto 
							SET nombre=:nombre, 
							inventario=:inventario, precio=:precio, peso=:peso, categoria=:categoria 
							WHERE id=$id");
		$update->bindValue('nombre',$producto->nombre);
		$update->bindValue('inventario',$producto->inventario);
		$update->bindValue('precio',$producto->precio);
		$update->bindValue('peso',$producto->peso);
		$update->bindValue('categoria',$producto->categoria);
		$update->execute();
	}

	public static function delete($id){
		$db=Db::getConnect();
		$delete=$db->prepare('DELETE  FROM producto WHERE id=:id');
		$delete->bindValue('id',$id);
		$delete->execute();		
	}
}

?>