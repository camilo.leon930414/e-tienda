

<div class="container">
	<h2>Lista Productos</h2>
	<div class="table-responsive">
		<table class="table table-hover table-dark">
			<thead class="thead-dark">
				<tr>
					<th>ID</th>
					<th>NOMBRE</th>
					<th>REFERENCIA</th>
					<th>INVENTARIO</th>
					<th>PRECIO</th>
					<th>PESO</th>
					<th>CATEGORIA</th>
					<th>FECHA DE CREACION</th>
					<td>Acciones</td>
				</tr>
				<tbody>
					<?php foreach ($listaProductos as $producto) { ?>
					<tr>
						<td><?php echo $producto['id']; ?></td>
						<td><?php echo $producto['nombre']; ?></td>
						<td><?php echo $producto['referencia']; ?></td>
						<td><?php echo $producto['inventario']; ?></td>
						<td><?php echo $producto['precio']; ?></td>
						<td><?php echo $producto['peso']; ?></td>
						<td><?php echo $producto['categoria']; ?></td>
						<td><?php echo $producto['created_at']; ?></td>
						<td>
						<a class="btn btn-warning"href="?controller=producto&&action=updateshow&&idProducto=<?php echo $producto['id'];?>">Editar</a>
						<a class="btn btn-danger"href="?controller=producto&&action=delete&&id=<?php echo $producto['id']; ?>">Eliminar</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>

			</thead>
		</table>

	</div>	

</div>