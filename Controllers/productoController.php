<?php 
/**
* 
*/
class productoController
{
	
	function __construct()
	{
		
	}

	function index(){
		require_once('Views/Producto/bienvenido.php');
	}

	function register(){
		require_once('Views/producto/register.php');
	}

	function save(){
		
		$producto = new Producto();
		$producto->nombre = $_POST['nombre'];
		$producto->referencia = $this->generateSKU();
		$producto->inventario = $_POST['inventario'];
		$producto->precio = $_POST['precio'];
		$producto->peso =  $_POST['peso'];
		$producto->categoria =  $_POST['categoria'];

		Producto::save($producto);

		$this->show();
	}

	function show(){
		$listaProductos = Producto::all();
		
		require_once('Views/producto/show.php');
	}

	function generateSKU(){
		$input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$input_length = strlen($input);
		$random_string = '';
		for($i = 0; $i < 15; $i++) {
			$random_character = $input[mt_rand(0, $input_length - 1)];
			$random_string .= $random_character;
		}
		return $random_string;
	}

	function updateshow(){
		$id=$_GET['idProducto'];
		$producto =Producto::searchById($id);
		$producto= $producto[0];
		require_once('Views/producto/updateshow.php');
	}

	function update(){
		$producto = new Producto();
		$producto->id = $_POST['id'];
		$producto->nombre = $_POST['nombre'];
		$producto->inventario = $_POST['inventario'];
		$producto->precio = $_POST['precio'];
		$producto->peso =  $_POST['peso'];
		$producto->categoria = $_POST['categoria'];
		Producto::update($producto);
		$this->show();
	}
	function delete(){
		$id=$_GET['id'];
		Producto::delete($id);
		$this->show();
	}

	function search(){
		if (!empty($_POST['id'])) {
			$id=$_POST['id'];
			$producto=Producto::searchById($id);
			$listaProductos[]=$producto;
			//var_dump($id);
			//die();
			require_once('Views/Producto/show.php');
		} else {
			$listaProductos=Producto::all();

			require_once('Views/Producto/show.php');
		}
		
		
	}

	function error(){
		require_once('Views/Producto/error.php');
	}

}

?>