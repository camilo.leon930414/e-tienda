<div class="container">
  <h2>Registro de Producto</h2>
  <form action="?controller=producto&&action=save" method="POST">
    <div class="form-group">
      <label for="text">Nombre:</label>
      <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" required>
    </div>
    <div class="form-group">
      <label for="text">Inventario:</label>
      <input type="number" class="form-control" id="inventario" placeholder="cantidad" name="inventario" required>
    </div>
    <div class="form-group">
      <label for="text">Precio:</label>
      <input type="number" class="form-control" id="precio" placeholder="valor" name="precio" required>
    </div>
    <div class="form-group">
      <label for="text">Peso:</label>
      <input type="number" class="form-control" id="peso" placeholder="peso" name="peso" required>
    </div>
    <div class="form-group">
        <label for="text">Categoria:</label>
        <select class="form-control" name="categoria" id="categoria" required>
          <option value="" disabled selected>Seleccione...</option>
          <option value="tecnologia">Tecnologia</option>
          <option value="comida">Comida</option>
          <option value="ropa">Ropa</option>
          <option value="aseo">Aseo</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Guardar</button>
  </form>
</div>