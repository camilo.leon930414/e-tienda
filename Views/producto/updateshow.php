<div class="container">
	<h2>Actualizar Producto</h2>
	<form action="?controller=producto&&action=update" method="POST">
		<input type="hidden" name="id" value="<?php echo $producto['id'] ?>" >
		<input type="hidden" name="referencia" value="<?php echo $producto['referencia'] ?>" >
		<div class="form-group">
		<label for="text">Nombre:</label>
		<input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" value="<?php echo $producto['nombre'] ?>">
		</div>
		<div class="form-group">
		<label for="text">Inventario:</label>
		<input type="number" class="form-control" id="inventario" placeholder="cantidad" name="inventario" value="<?php echo $producto['inventario'] ?>">
		</div>
		<div class="form-group">
		<label for="text">Precio:</label>
		<input type="number" class="form-control" id="precio" placeholder="valor" name="precio" value="<?php echo $producto['precio'] ?>">
		</div>
		<div class="form-group">
		<label for="text">Peso:</label>
		<input type="number" class="form-control" id="peso" placeholder="peso" name="peso" value="<?php echo $producto['peso'] ?>">
		</div>
		<div class="form-group">
			<label for="text">Categoria:</label>
			<select class="form-control" name="categoria" id="categoria">
			<option value="tecnologia" <?php if($producto['categoria'] =="tecnologia" ){echo 'selected';} ?> >Tecnologia</option>
			<option value="comida"<?php if($producto['categoria'] =="comida" ){echo 'selected';} ?> >Comida</option>
			<option value="ropa" <?php if($producto['categoria'] =="ropa" ){echo 'selected';} ?> >Ropa</option>
			<option value="aseo" <?php if($producto['categoria'] =="aseo" ){echo 'selected';} ?> >Aseo</option>
			</select>
		</div>
		<button type="submit" class="btn btn-primary">Actualizar</button>
	</form>
</div>